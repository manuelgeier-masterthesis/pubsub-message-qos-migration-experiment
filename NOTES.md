


# Scripts

 - build-broker.sh
   Builds the broker and copies the final artifacts to the docker folder.

 - build-client.sh
   Builds the client and copies the final artifacts to the docker folder.

 - build-docker-image.sh
   Build the docker image.

 - run-docker-compose.sh
   Start docker containers.

 - docker-compose stop
   Stop docker containers.

 - image-files.sh
   Log into a container with sh.


# Install
   
Install pumba on virtual machine and make it executable:
docker@default:~$ wget https://github.com/alexei-led/pumba/releases/download/0.6.2/pumba
docker@default:~$ chmod +x pumba
docker@default:~$ docker pull gaiadocker/iproute2

# Experiments

in folder experiment/
each has a unique number/folder (e.g. 01, 02,...)
contains ocnfiguration for brokers, publishers and subscribers
on preparation, the configs will be copied to the docker-machine-shared-folder where it will be picked up by the right Client
after the experiment, the output will be copied into the result folder of the experiment
further analysis and plots will be created and stored in the result folder as well

run-experiment.sh NR
-> run an experiminet
prepare-experiment.sh NR
-> copy experiment files to the shared folder

# SSH into a Container

1. Use docker ps to get the name of the existing container.
2. Use the command docker exec -it <container name> /bin/bash to get a bash shell in the container.
   Generically, use docker exec -it <container name> <command> to execute whatever command you specify in the container.


# Build releases

## moquette

./gradlew clean distribution:distMoquetteZip

Output: ./distribution/build/moquette-0.11-SNAPSHOT.zip

## netty-mqtt

mvn clean install assembly:single

Output: .\target\netty-mqtt-RELEASE.jar




docker-compose stop; ./build-docker-image.sh && ./run-docker-compose.sh && winpty docker exec -it mydocker_broker_2_1 bash


winpty docker exec -it mydocker_broker_2_1 /scripts/broker_1-init.sh

winpty docker exec -it mydocker_broker_1_1 bash -c "comcast --device=eth0 --latency=100 --target-addr=172.172.3.1"
winpty docker exec -it mydocker_broker_2_1 bash -c "comcast --device=eth0 --latency=500 --target-addr=172.172.3.1"

winpty docker exec -it mydocker_broker_1_1 bash -c "comcast --device=eth0 --latency=500 --target-addr=172.172.3.1"
winpty docker exec -it mydocker_broker_2_1 bash -c "comcast --device=eth0 --latency=100 --target-addr=172.172.3.1"



Setup network conditions:
docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.1.2 delay --time 1000 mydocker_broker_1_1
docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.3.1 delay --time 2000 mydocker_broker_1_1

docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.1.1 delay --time 1000 mydocker_broker_2_1
docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.3.1 delay --time 500 mydocker_broker_2_1

docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.1.1 delay --time 2000 mydocker_subscriber_1_1
docker run -itd --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba pumba netem --duration 30s --target 172.172.1.2 delay --time 500 mydocker_subscriber_1_1
