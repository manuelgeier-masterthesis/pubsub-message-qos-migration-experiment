if ! ./experiment-exists.sh $1; then
    exit 1
fi
echo_h1 () {
  echo ""
  echo ""
  echo " $1"
  echo "==============================================================="
}

EXPERIMENT=$1

echo_h1 "Prepare experiment $EXPERIMENT"
rm -rf docker-machine-shared-folder/*
cp -r docker-machine-shared-folder_defaults/* docker-machine-shared-folder/
#cp -r experiment/_defaults/* docker-machine-shared-folder/
mkdir docker-machine-shared-folder/Publisher_001
mkdir docker-machine-shared-folder/Publisher_002
mkdir docker-machine-shared-folder/Subscriber_001
cp experiment/$EXPERIMENT/Publisher_001/config.properties docker-machine-shared-folder/Publisher_001/
cp experiment/$EXPERIMENT/Publisher_002/config.properties docker-machine-shared-folder/Publisher_002/
cp experiment/$EXPERIMENT/Subscriber_001/config.properties docker-machine-shared-folder/Subscriber_001/
