if [ $# -ne 1 ]
then
    echo "EXPERIMENT param missing"
    exit 1
fi

EXPERIMENT=$1

if [ ! -d experiment/$EXPERIMENT ]
then
    echo "experiment $EXPERIMENT does not exist"
    exit 1
fi
