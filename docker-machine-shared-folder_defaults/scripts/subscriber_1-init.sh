#!/bin/sh
#
# Network:
#
#     B1    <- 1000 ->   B2
#     ^                  ^
#    2000               500
#     |                  v
#     +--------------->Client
#

# Client -> B1
#comcast --device=eth0 --latency=2000 --target-addr=172.172.1.1

# Client -> B2
#comcast --device=eth0 --latency=500 --target-addr=172.172.1.2
