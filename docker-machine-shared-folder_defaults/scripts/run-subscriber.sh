#!/bin/sh

CONFIG_FILE=/shared/config.properties
LOG_CONFIG_FILE=/artifacts/client/log.properties

# Arguments
INIT_SCRIPT=$1

echo "Start SUBSCRIBER"
echo "Init Script:   $INIT_SCRIPT"
echo "Configuration: $CONFIG_FILE"
cat $CONFIG_FILE

cd "$(dirname "$0")"

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Set JavaHome if it exists
if [ -f "${JAVA_HOME}/bin/java" ]; then
   JAVA=${JAVA_HOME}/bin/java
else
   JAVA=java
fi
export JAVA

echo "--- Init script started ---"
bash -c $INIT_SCRIPT
echo "--- Init script finished ---"


JAR=/artifacts/client/pubsub-message-qos-migration-experiment-1.0-SNAPSHOT-all.jar
MAIN=io.geier.diplomathesis.experiment.main.SubscriberClient
PARAMS=$CONFIG_FILE

echo "Start..."
$JAVA -Dlog4j.configuration="file:$LOG_CONFIG_FILE" -cp $JAR $MAIN $PARAMS
