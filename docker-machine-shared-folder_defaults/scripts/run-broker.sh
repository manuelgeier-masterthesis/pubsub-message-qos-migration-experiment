#!/bin/sh

CONFIG_FILE=/artifacts/moquette/config/moquette.conf
LOG_CONFIG_FILE=/artifacts/moquette/config/moquette-log.properties

# Arguments
HOST=$1
INIT_SCRIPT=$2

echo "Start BROKER"
echo "Host: $HOST"
echo "Init Script: $INIT_SCRIPT"

echo "Replace properties..."
# set the correct host to be contacted for others
sed -i "/broker.host 0.0.0.0/c broker.host $HOST" $CONFIG_FILE
sed -i "/migration.metrics false/c migration.metrics true" $CONFIG_FILE
# adjust logging
sed -i "/log4j.logger.io.moquette.spi.impl.ProtocolProcessorBootstrapper=WARN/c log4j.logger.io.moquette.spi.impl.ProtocolProcessorBootstrapper=INFO" $LOG_CONFIG_FILE
sed -i "/log4j.logger.io.geier.diplomathesis=WARN/c log4j.logger.io.geier.diplomathesis=DEBUG" $LOG_CONFIG_FILE
sed -i "/log4j.appender.file.Threshold=INFO/c log4j.appender.file.Threshold=DEBUG" $LOG_CONFIG_FILE
sed -i "/log4j.appender.file.File=moquette.log/c log4j.appender.file.File=/shared/output/moquette.log" $LOG_CONFIG_FILE
sed -i "/log4j.appender.file.MaxFileSize=1MB/c log4j.appender.file.MaxFileSize=100MB" $LOG_CONFIG_FILE
sed -i "/log4j.appender.file.MaxBackupIndex=1/c log4j.appender.file.MaxBackupIndex=10" $LOG_CONFIG_FILE
echo "Done!"

echo "--- Init script started ---"
bash -c $INIT_SCRIPT
echo "--- Init script finished ---"

/artifacts/moquette/bin/moquette.sh
