#!/bin/sh
#
# Network:
#
#     B1    <- 1000 ->   B2
#     ^                  ^
#    2000               500
#     |                  v
#     +--------------->Client
#

# B1 -> B2
#comcast --device=eth0 --latency=1000 --target-addr=172.172.1.2

# B1 -> Client
#comcast --device=eth0 --latency=2000 --target-addr=172.172.3.1
