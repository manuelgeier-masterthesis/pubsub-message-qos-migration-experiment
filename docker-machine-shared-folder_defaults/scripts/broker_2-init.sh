#!/bin/sh
#
# Network:
#
#     B1    <- 1000 ->   B2
#     ^                  ^
#    2000               500
#     |                  v
#     +--------------->Client
#

# B2 -> B1
#comcast --device=eth0 --latency=1000 --target-addr=172.172.1.1

# B2 -> Client
#comcast --device=eth0 --latency=500 --target-addr=172.172.3.1
