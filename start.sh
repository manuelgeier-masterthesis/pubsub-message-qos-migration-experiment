./stop.sh

docker-compose -f docker-compose.yml up --build -d
sleep 5s

#./setup-network.sh


echo "Start brokers..."
docker exec mydocker_broker_1_1 bash -c "sh /shared-all/scripts/run-broker.sh 172.172.1.1 /shared-all/scripts/broker_1-init.sh &> /dev/null" &
docker exec mydocker_broker_2_1 bash -c "sh /shared-all/scripts/run-broker.sh 172.172.1.2 /shared-all/scripts/broker_2-init.sh &> /dev/null" &
sleep 10s

echo "Start publishers..."
docker exec mydocker_publisher_1_1 bash -c "sh /shared-all/scripts/run-publisher.sh &> /dev/null" &
docker exec mydocker_publisher_2_1 bash -c "sh /shared-all/scripts/run-publisher.sh &> /dev/null" &
sleep 5s

echo "Start subscriber..."
docker exec mydocker_subscriber_1_1 bash -c "sh /shared-all/scripts/run-subscriber.sh /shared-all/scripts/subscriber_1-init.sh &> /dev/null" &

sleep 5
echo "All systems started!"
