if ! ./experiment-exists.sh $1; then
    exit 1
fi
echo_h1 () {
  echo ""
  echo ""
  echo " $1"
  echo "==============================================================="
}
echo_h2 () {
  echo ""
  echo " $1"
  echo "---------------------------------------------------------------"
}

RESULT_DIR=experiment/$1
R_SCRIPT_DIR=../../experiment-scripts

echo_h1 "Finalize experiment $RESULT_DIR"
pwd
echo_h2 "Results: ${pwd}$RESULT_DIR"

cat $RESULT_DIR/Broker_001/output/moquette.log* | grep "Send migration packet.\|Migration packet received." > $RESULT_DIR/Broker_001/output/packets.log
cat $RESULT_DIR/Broker_002/output/moquette.log* | grep "Send migration packet.\|Migration packet received." > $RESULT_DIR/Broker_002/output/packets.log
cat $RESULT_DIR/Subscriber_001/output/client.log* | grep "Send migration packet.\|Migration packet received." > $RESULT_DIR/Subscriber_001/output/packets.log
cat $RESULT_DIR/Subscriber_001/output/client.log* | grep "DataIntegrityVerifier" > $RESULT_DIR/Subscriber_001/output/DataIntegrityVerifier.log
echo "DataIntegrityVerifier tail logs:"
tail $RESULT_DIR/Subscriber_001/output/DataIntegrityVerifier.log

echo_h2 "message_latency.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_latency.R)
echo_h2 "message_latency_bytime.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_latency_bytime.R)
echo_h2 "message_arrival.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_arrival.R)
#echo_h2 "message_transfer.R"
#(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_transfer.R)
#echo_h2 "message_trip.R"
#(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_trip.R)
echo_h2 "message_sequence.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_sequence.R)
echo_h2 "message_time.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_time.R > time.txt)
echo_h2 "message_time2.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_time2.R)
echo_h2 "message_time_dep_arr.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_time_dep_arr.R)
echo_h2 "message_cdf.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_cdf.R)
echo_h2 "message_loss_dupl.R"
(cd $RESULT_DIR; Rscript --vanilla $R_SCRIPT_DIR/message_loss_dupl.R > loss_dupl.txt)
(cd $RESULT_DIR; cat loss_dupl.txt | grep "Topic:\|Check:" > experiment_result.txt; cat experiment_result.txt)
echo_h2 "message_times.R"
(cd experiment; Rscript --vanilla ../experiment-scripts/message_times.R)

(cd $RESULT_DIR; cat Broker_001/output/moquette.log* | grep "stored" -wc | awk '{print "Stored: "$1}' >> experiment_result.txt)


# check for errors
ERROR_FILE=$RESULT_DIR/errors.txt
[ -e $ERROR_FILE ] && rm $ERROR_FILE

if ! grep -q "MigAckPacket" $RESULT_DIR/Broker_001/output/packets.log; then
    echo "Error: Migration not finished: $RESULT_DIR" >> $ERROR_FILE
fi

(cd experiment; egrep -r --include=loss_dupl.txt "Check:  Invalid") >> $ERROR_FILE

echo_h2 "Errors of $RESULT_DIR"
if ! [ -s $ERROR_FILE ]
then
    echo "Success!"
else
    cat $ERROR_FILE
fi


# add errors to global errors
GLOBAL_ERROR_FILE=experiment/experiment-errors.txt
[ -e $GLOBAL_ERROR_FILE ] && rm $GLOBAL_ERROR_FILE
(cd experiment; egrep -r --include=errors.txt "Error:") >> $GLOBAL_ERROR_FILE
(cd experiment; egrep -r --include=loss_dupl.txt "Check:  Invalid") >> $GLOBAL_ERROR_FILE

echo_h1 "Global Errors"
if ! [ -s $GLOBAL_ERROR_FILE ]
then
    echo "Success!"
else
    cat $GLOBAL_ERROR_FILE
fi
