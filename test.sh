
DOCKER_MACHINE_STATUS=$(docker-machine status)
if [ "$DOCKER_MACHINE_STATUS" != "Running" ]
then
  echo "Docker Machine is not running. Starting machine..."
  docker-machine start
fi
