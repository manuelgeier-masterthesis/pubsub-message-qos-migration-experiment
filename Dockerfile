
FROM openjdk:8-jdk-alpine

RUN apk update && apk add --no-cache dumb-init su-exec shadow ca-certificates iproute2 unzip bash git ethtool ipfw iptables ip6tables sudo

# copy files
COPY docker/docker_entrypoint.sh /docker_entrypoint.sh
COPY artifacts/ /artifacts/

# extract moquette dist
RUN unzip /artifacts/moquette/moquette-0.11-SNAPSHOT.zip -d /artifacts/moquette/
RUN rm /artifacts/moquette/moquette-0.11-SNAPSHOT.zip
COPY artifacts/moquette-config/ /artifacts/moquette/config/

ENTRYPOINT ["dumb-init", "/docker_entrypoint.sh"]
#CMD ["/artifacts/moquette-dist/bin/moquette.sh"]
