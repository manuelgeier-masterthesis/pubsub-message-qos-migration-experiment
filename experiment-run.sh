if ! ./experiment-exists.sh $1; then
    exit 1
fi

EXPERIMENT=$1

RUNS=1
if [ $# -ge 2 ] && [ $2 -ge 1 ]
then
  RUNS=$2
fi

START_RUN=1
if [ $# -ge 3 ] && [ $3 -ge 1 ]
then
  START_RUN=$3
fi

echo_h1 () {
  echo ""
  echo ""
  echo " $1"
  echo "==============================================================="
}
echo_h2 () {
  echo ""
  echo " $1"
  echo "---------------------------------------------------------------"
}

DOCKER_MACHINE_STATUS=$(docker-machine status)
if [ "$DOCKER_MACHINE_STATUS" != "Running" ]
then
  echo "Docker Machine is not running. Starting machine..."
  docker-machine start
fi

echo_h1 "Experiment $EXPERIMENT started"
starttime=`date +%s.%N`

# just in case, stop current instance
./stop.sh

echo $START_RUN
echo $RUNS
echo `seq $START_RUN $RUNS`

# Runs
for i in `seq $START_RUN $RUNS`;
do
    echo_h1 "Run $i of $RUNS started"

    ./experiment-prepare.sh $EXPERIMENT

    echo_h1 "Run experiment $EXPERIMENT (run $i of $RUNS)"
    ./start.sh

    # after 10 seconds every service will start
    # we wait some time to get the services running before moving the subscriber
    echo "Wait..."
    sleep 15s
    ./migrate.sh
    echo "Wait..."
    sleep 40s
    #sleep 90s
    #sleep 200s
    ./stop.sh

    echo "Gather experiment $EXPERIMENT output"
    RESULT="$EXPERIMENT-run-$i-result"
    RESULT_DIR="experiment/$RESULT"
    rm -rf $RESULT_DIR
    cp -r docker-machine-shared-folder $RESULT_DIR
    rm -r $RESULT_DIR/scripts
    cp experiment/$EXPERIMENT/*.r.csv $RESULT_DIR/

    ./experiment-post.sh $RESULT

    # since we set the network conditions and the stop automatically
    echo "Cooldown..."
    sleep 30s

    echo_h2 "Result"
    cat $RESULT_DIR/experiment_result.txt

    echo_h1 "Run $i of $RUNS finished"
done


endtime=`date +%s.%N`
runtime=$( echo "$endtime - $starttime" | bc -l )
echo "Experiment $EXPERIMENT finished in ${runtime} seconds"
