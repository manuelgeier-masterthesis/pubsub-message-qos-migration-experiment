PROJECTDIR=pubsub-message-qos-migration-experiment
OUTDIR=./artifacts/client/

echo "Build Client..."
(cd ../../$PROJECTDIR; ./gradlew clean fatJar)
echo "Copy artifact to $OUTDIR ..."
mkdir --parents $OUTDIR
cp ../../$PROJECTDIR/build/libs/pubsub-message-qos-migration-experiment-1.0-SNAPSHOT-all.jar $OUTDIR
echo "Done!"
