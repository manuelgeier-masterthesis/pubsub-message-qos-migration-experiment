
for d in experiment/*/ ; do
    foldername=$(echo "$d" | cut -d'/' -f 2)
    if [[ $foldername = *-result ]]; then
      experiment=$foldername
      echo "$experiment"
      ./experiment-post.sh $experiment
    fi
done
