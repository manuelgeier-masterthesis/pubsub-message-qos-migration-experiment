
LOG1=/shared/setup-network.1.log
LOG2=/shared/setup-network.2.log
LOG3=/shared/setup-network.3.log
LOG4=/shared/setup-network.4.log
LOG5=/shared/setup-network.5.log
LOG6=/shared/setup-network.6.log

DURATION=100s

echo "Copy pumba to docker machine..."
docker-machine ssh default "cp /shared/scripts/pumba ~/"

echo "Setup network conditions..."
# Network:
#     B1   <-  40  ->   B2
#     ^                 ^
#     40                10
#     |                 v
#     +-------------->Client
#broker 1
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.2 delay --time 50 mydocker_broker_1_1" &
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.3.1 delay --time 100 mydocker_broker_1_1" &
#broker 2
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.1 delay --time 50 mydocker_broker_2_1" &
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.3.1 delay --time 25 mydocker_broker_2_1" &
#subscriber
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.1 delay --time 100 mydocker_subscriber_1_1" &
#docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.2 delay --time 25 mydocker_subscriber_1_1" &

# we can only set one delay for a single container, for a target :(
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.3.1 delay --time 40 -j 5 mydocker_broker_1_1" &
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.3.1 delay --time 10 -j 5 mydocker_broker_2_1" &
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.2 delay --time 10 -j 5 mydocker_subscriber_1_1" &
sleep 2s
# we open other targets as well to get some delay between them
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.2 delay --time 5000 -j 5 mydocker_broker_1_1" &
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.1 delay --time 5000 -j 5 mydocker_broker_2_1" &
docker-machine ssh default "~/pumba netem --tc-image gaiadocker/iproute2 --duration $DURATION --target 172.172.1.1 delay --time 10000 -j 5 mydocker_subscriber_1_1" &


sleep 8s
