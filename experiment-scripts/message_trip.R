
df1 = read.csv("Publisher_001/output/metrics.csv", header=TRUE)
df2 = read.csv("Publisher_002/output/metrics.csv", header=TRUE)
df3 = read.csv("Subscriber_001/output/metrics.csv", header=TRUE)
d <- rbind(df1, df2, df3)

t <- cbind("topic1", "topic2", "topic3")


#normalize timestamps
d$timestamp = d$timestamp - min(d$timestamp)

# split data
d_depart <- d[d$type=="PRODUCED",]
names(d_depart)[names(d_depart) == 'clientId'] <- 'producerClientId'
d_arrive <- d[d$type=="RECEIVED",]
names(d_arrive)[names(d_arrive) == 'clientId'] <- 'receiverClientId'

# merge received messages to produced timestamps
d2 <- merge(d_arrive, d_depart[ , c("guid", "broker", "timestamp", "producerClientId")], by=c("guid", "broker"), suffixes=c("_received", "_produced"), all.x=TRUE)
d2$latency <- d2$timestamp_received - d2$timestamp_produced

# remove type column
d2 <- subset(d2, select = -c(type) )

d2$latency <- d2$timestamp_received - d2$timestamp_produced

plot_it <- function(depart, arrive) {
  len <- length(depart)
  depart_l <- rep(1,len)
  arrive_l <- rep(0,len)
  plot(c(depart, arrive), c(depart_l, arrive_l), type="n", yaxt='n', ylab= "", xlab="time (ms)")
  axis(2, at=c(0, 1),labels=c("depart", "arrive"), las=2)
  points(depart, depart_l, col="orange", pch=1)
  points(arrive, arrive_l, col="orange", pch=16)
  arrows(depart, depart_l, arrive, arrive_l, col="orange", lty="dashed", lwd=0.5, length=0.1,angle=35)
}

parBak <- par()
#pdf("message-trip.pdf")
#png("message-trip.png")

#layout(c(2,1))
par(mfrow=c(2,1)) # all plots on one page

ranks <- order(d2$timestamp_produced)
plot(d2$timestamp_produced[ranks], d2$latency[ranks], type="n", xlab="depart time (ms)", ylab="delay (ms)")
lines(d2$timestamp_produced[ranks], d2$latency[ranks], col="gray")
b1 <- d2[d2$broker == "172.172.1.1:1883",]
b2 <- d2[d2$broker == "172.172.1.2:1883",]
points(b1$timestamp_produced[ranks], b1$latency[ranks], pch=16, col="red")
points(b2$timestamp_produced[ranks], b2$latency[ranks], pch=16, col="blue")

plot_it(d2$timestamp_produced, d2$timestamp_received)

#dev.off()
par(parBak)
