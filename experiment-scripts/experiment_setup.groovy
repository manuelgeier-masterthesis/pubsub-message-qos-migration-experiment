import static groovy.json.JsonOutput.*

def list = []

def dir = new File("./")
dir.eachDir () { file ->
	if(!file.name.endsWith("-result")) list << file
}

def configsMap = [:]

list.each { configDir -> 

	def expName = configDir.name
	
	def publisher001ConfigFile = new File(configDir.path, "Publisher_001/config.properties")
	def publisher002ConfigFile = new File(configDir.path, "Publisher_002/config.properties")
	def subscriber001ConfigFile = new File(configDir.path, "Subscriber_001/config.properties")

	//subscriptions=topicA:0,topicB:1,topicC:2

	if(!configsMap.containsKey(expName)) {
		configsMap.put(expName, [topics: [:]])
	}
	configMap = configsMap[expName]
	
	subscriber001ConfigFile.eachLine { line ->
		if(!line.startsWith("subscriptions=")) return
		
		def topicsConfig= line.substring("subscriptions=".length()).split(",")
		
		topicsConfig.each { topicConfig ->
			def config = topicConfig.split(":")
			
			def topic = config[0]
			def qos = config[1]
			
			if(!configMap.topics.containsKey(topic)) {
				configMap.topics.put(topic, [qos: "QoS $qos",
					pub001Interval: null, pub001Delay: null,
					pub002Interval: null, pub002Delay: null, 
				])
			}
		}
	}
	
	publisher001ConfigFile.eachLine { line ->
		if(!line.startsWith("topics=")) return
		
		def topicsConfig= line.substring("topics=".length()).split(",")
		
		topicsConfig.each { topicConfig ->
			def config = topicConfig.split(":")
			
			def topic = config[0]
			def qos = config[1]
			def interval = config[2]
			def delay = config[3]
			
			configMap.topics[topic].pub001Interval = interval
			configMap.topics[topic].pub001Delay = delay
		}
	}
	
	publisher002ConfigFile.eachLine { line ->
		if(!line.startsWith("topics=")) return
		
		def topicsConfig= line.substring("topics=".length()).split(",")
		
		topicsConfig.each { topicConfig ->
			def config = topicConfig.split(":")
			
			def topic = config[0]
			def qos = config[1]
			def interval = config[2]
			def delay = config[3]
			
			configMap.topics[topic].pub002Interval = interval
			configMap.topics[topic].pub002Delay = delay
		}
	}
	
}

println prettyPrint(toJson(configsMap))


configsMap.each { expName, expConfig ->
	def expNameNorm = expName.replaceAll("_", "\\\\_")
	
	println "\\multicolumn{6}{l}{\\textbf{$expNameNorm}}\\\\"
	
	expConfig.topics.each { topic, topicConfig ->
		println "    $topic & $topicConfig.qos & $topicConfig.pub001Interval & $topicConfig.pub001Delay & $topicConfig.pub002Interval & $topicConfig.pub002Delay\\\\"
	}
    
	println("\\hdotted")
	println()
}

