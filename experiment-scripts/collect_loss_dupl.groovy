import static groovy.json.JsonOutput.*

def list = []

def dir = new File("./")
dir.eachFileRecurse () { file ->
	if(file.name == "experiment_result.txt") list << file
}

def resultsMap = [:]

list.each { resultFile -> 

	def folderName = resultFile.getParentFile().getName();
	
	groups = (folderName =~ /(.+)-run-(\d+)-result/)
	def expName = groups[0][1]
	def expRun = groups[0][2]
	
	if(!resultsMap.containsKey(expName)) {
		resultsMap.put(expName, [topics: [:]])
	}
	resultMap = resultsMap[expName]

	resultFile.eachLine { line ->
		if(!line.startsWith("Check")) return
		//println line

		//state, topic, qos, dupl, loss
		groups = (line =~ /Check:  (.+)   (.+)   (.+)   Dupl: (\d+)   Loss: (\d+)/)
		
		def state = groups[0][1]
		def topic = groups[0][2]
		def qos  = groups[0][3]
		def dupl = groups[0][4]
		def loss = groups[0][5]
		
		if(!resultMap.topics.containsKey(topic)) {
			resultMap.topics.put(topic, [qos: qos, valid: true, loss: 0, dupl: 0])
		}
		resultMapTopic = resultMap.topics[topic]
			
		resultMapTopic.valid &= state == "Valid"
		resultMapTopic.loss += loss.toInteger()
		resultMapTopic.dupl += dupl.toInteger()
	}
}

println prettyPrint(toJson(resultsMap))


resultsMap.each { expName, expResult ->
	println expName.replaceAll("_", "\\\\_")
	expResult.topics.each { topic, topicRes ->
		println "    & $topic & $topicRes.qos & $topicRes.loss & $topicRes.dupl & ${topicRes.valid ? "\\checkmark" : "\\xmark"} \\\\"
	}
	println("\\hdotted")
	println()
}

