RUNS=1
if [ $# -eq 1 ] && [ $1 -ge 1 ]
then
  RUNS=$1
fi

starttime=`date +%s.%N`

for d in experiment/*/ ; do
    foldername=$(echo "$d" | cut -d'/' -f 2)
    if [[ $foldername != _* && $foldername != *result ]]; then
      experiment=$foldername
      echo "$experiment"
      ./experiment-run.sh $experiment $RUNS
    fi
done

endtime=`date +%s.%N`
runtime=$( echo "$endtime - $starttime" | bc -l )
echo "start   : $starttime"
echo "end     : $endtime"
echo "duration: $runtime"
