echo "Build Broker..."
(cd ../../moquette; ./gradlew clean distribution:distMoquetteZip)
echo "Copy artifact to ./artifacts/moquette/ ..."
cp ../../moquette/distribution/build/moquette-0.11-SNAPSHOT.zip ./artifacts/moquette/
echo "Done!"
